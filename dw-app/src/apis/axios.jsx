import axios from "axios";

// server url with port 3080
const instance = axios.create({
  baseURL: "http://localhost:3080/api",
});

export default instance;

// baseURL: "https://mongo-realtime.herokuapp.com",
