import socketIOClient from "socket.io-client";

const serverEndPoint = "http://localhost:3080/";

const socket = socketIOClient(serverEndPoint, {
  transports: ["websocket"],
});

export default socket;
