import "./App.css";
import React, { Fragment, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./Home";
import Register from "./auth/Register";
import Login from "./auth/Login";
import PrivateRoute from "./routing/PrivateRoute";

const App = () => {
  const [isAuth, setIsAuth] = useState(localStorage.getItem("token") !== null);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));

  return (
    <Router>
      <Fragment>
        <Routes>
          <Route exact path="/" element={<PrivateRoute isAuth={isAuth} />}>
            <Route
              exact
              path="/"
              element={
                <Home setIsAuth={setIsAuth} setUser={setUser} user={user} />
              }
            />
          </Route>
          <Route
            exact
            path="/register"
            element={<Register setIsAuth={setIsAuth} setUser={setUser} />}
          />
          <Route
            exact
            path="/login"
            element={<Login setIsAuth={setIsAuth} setUser={setUser} />}
          />
        </Routes>
      </Fragment>
    </Router>
  );
};
export default App;
