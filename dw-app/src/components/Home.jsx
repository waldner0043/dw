import React, { useState, useEffect, useRef } from 'react';
import '../styles/css/antd.css';
import './App.css';
import LeftSideBar from '../../../dw-client/components/LeftSideBar';
import ChatBox from './ChatBox';
import RightSideBar from './RightSideBar';
import { Layout } from 'antd';
// import Pusher from "pusher-js";
import axios from '../apis/axios';
import { io } from 'socket.io-client';
import GetLoginHeader from '../utils/GetLoginHeader';

const Home = ({ setIsAuth, setUser, user }) => {
    const [selectedChatroom, setSelectedChatroom] = useState({
        name: 'Welcome to DW Chat Room',
    });
    const [chatrooms, setChatrooms] = useState([]);
    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState(null);
    const [arrivalMessage, setArrivalMessage] = useState(null);
    const [activeMembers, setActiveMembers] = useState([]);
    const socket = useRef();

    // retrieve all chatrooms by the sepcific user
    useEffect(() => {
        axios.get('/chatrooms', GetLoginHeader()).then((response) => {
            setChatrooms(response.data);
            console.log(response.data);
        });
    }, []);

    // connect to socket.disconnect()
    useEffect(() => {
        socket.current = io('http://localhost:8900');
        socket.current.on('getMessage', (data) => {
            const message = { ...data };
            setArrivalMessage(message);
        });
    }, []);

    // get all messages within the specific chatroom
    useEffect(() => {
        if (selectedChatroom._id) {
            axios
                .get(
                    `/chatrooms/${selectedChatroom._id}/messages`,
                    GetLoginHeader()
                )
                .then((response) => {
                    setMessages(response.data);
                });
        }
        if (selectedChatroom.name !== 'Welcome to DW Chat Room') {
            const data = {
                chatRoomId: selectedChatroom._id,
                userId: user._id,
            };
            socket.current.emit('joinRoom', data);

            socket.current.on('getMembers', (users) => {
                setActiveMembers(users);
            });

            socket.current.on('removeMember', (removedMember) => {
                console.log(removedMember);
                console.log(activeMembers);
                setActiveMembers(
                    activeMembers.filter((member) => member !== removedMember)
                );
            });

            return () => {
                socket.current.emit('leaveRoom', selectedChatroom._id);
                socket.current.removeAllListeners('getMembers');
                socket.current.removeAllListeners('removeMember');
            };
        }
    }, [selectedChatroom]);

    // new message for user input
    useEffect(() => {
        if (newMessage) {
            console.log(newMessage);
            setMessages((prev) => [...prev, newMessage]);
            socket.current.emit('sendMessage', newMessage);
        }
    }, [newMessage]);

    // message from other users
    useEffect(() => {
        // console.log("arrivalMessage", arrivalMessage);
        arrivalMessage && setMessages((prev) => [...prev, arrivalMessage]);
    }, [arrivalMessage, selectedChatroom]);

    return (
        <div className='home'>
            <Layout>
                <LeftSideBar
                    setSelectedRoom={setSelectedChatroom}
                    setChatrooms={setChatrooms}
                    chatrooms={chatrooms}
                    setIsAuth={setIsAuth}
                    socket={socket.current}
                    setUser={setUser}
                />
                <Layout>
                    <ChatBox
                        setChatrooms={setChatrooms}
                        setSelectedChatroom={setSelectedChatroom}
                        selectedChatroom={selectedChatroom}
                        messages={messages}
                        setNewMessage={setNewMessage}
                        userId={user._id}
                    />
                </Layout>
                <RightSideBar
                    members={
                        selectedChatroom.members ? selectedChatroom.members : []
                    }
                    activeMemberIds={activeMembers}
                />
            </Layout>
        </div>
    );
};

export default Home;

//////
// listening chatroomsDB using pusher
// practicing purpose

// useEffect(() => {
//   const pusher = new Pusher("19313fc0aa5b7f33db1c", {
//     cluster: "ap4",
//   });

//   const channel = pusher.subscribe("chatrooms");
//   channel.bind("inserted", (newChatRoom) => {
//     setChatrooms([...chatrooms, newChatRoom]); // keep the existing and add a new one
//   });

//   return () => {
//     channel.unbind_all();
//     channel.unsubscribe();
//   };
// }, [chatrooms]);
