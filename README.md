# DW Chatting Room

##  App development

- Client
    - Written in [TypeScript](https://www.typescriptlang.org/)
    - Routing with [Next.js](https://nextjs.org/)
    - Using [React](https://reactjs.org/) framework with [Context API](https://reactjs.org/docs/context.html)

- Server
    - Written in [Node.js](https://nodejs.org/en/) with [Express](https://expressjs.com/) framework
    - Data is stored in [MongoDB Atlas](https://www.mongodb.com/atlas/database)
    - Authorised by [Passport-JWT](http://www.passportjs.org/packages/passport-jwt/)
    - Authenticated by [Passport-Authenticate](https://www.passportjs.org/concepts/authentication/middleware/)

- Socket
    - Using [Socket.IO](https://socket.io/)
