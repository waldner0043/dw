export interface Header {
	headers: { 'Content-Type': string; Authorization: string | null };
}

export interface UserRequest {
	email: string;
	username: string;
	password: string;
}

export interface ChatroomRequest {
	name?: string;
	memberId?: string;
	type: 'name' | 'addMember' | 'deleteMember';
}

export interface MessageRequest {
	content: string;
}
