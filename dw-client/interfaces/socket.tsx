import { Message } from '@/interfaces/model';

export interface ServerToClientEvents {
	getMembers: (users: string[]) => void;
	removeMember: (removedUser: string) => void;
	getMessage: (message: Message) => void;
}

export interface ClientToServerEvents {
	joinRoom: (data: { chatRoomId: string; userId: string }) => void;
	leaveRoom: (chatroomId: string) => void;
	sendMessage: (message: Message) => void;
}

export interface InterServerEvents {
	//ToDo
}

export interface SocketData {
	//ToDo
}
