export interface User {
	_id: string;
}

export interface UserObject {
	email: string;
	username: string;
	__v: number;
	_id: string;
}

export interface Chatroom {
	createdAt: Date;
	members: UserObject[];
	name: string;
	ownerId: string;
	updatedAt: Date;
	__v: number;
	_id: string;
}

export interface Message {
	chatRoomId: string;
	content: string;
	createdAt: Date;
	senderId: UserObject;
	updatedAt: Date;
	__v: number;
	_id: string;
}
