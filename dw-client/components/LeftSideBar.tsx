import React, { useContext, useState } from 'react';
import { Layout } from 'antd';
import LeftSideBarHeader from '@/components/LeftSideBarHeader';
import LeftSideBarContent from './LeftSideBarContent';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';
import { Chatroom } from '@/interfaces/model';

const { Sider } = Layout;

const LeftSideBar = () => {
	const { chatrooms, searchedChatroomName } = useContext(ChatroomContext) as ChatroomContextType;
	const [isCollapsed, setIsCollapsed] = useState(false);

	const roomsFilter = (chatroom: Chatroom) => {
		const chatroomsLength = searchedChatroomName.length;
		return searchedChatroomName === chatroom.name.substring(0, chatroomsLength);
	};

	const handleOnBreakpoint = (broken: boolean) => {
		setIsCollapsed(broken);
	};

	return (
		<React.Fragment>
			<Sider
				className="left-sider"
				width={'20%'}
				theme="light"
				breakpoint="xl"
				onBreakpoint={handleOnBreakpoint}
				collapsed={isCollapsed}
				collapsedWidth={'25%'}
			>
				<LeftSideBarHeader />
				<LeftSideBarContent
					chatroomsMatched={searchedChatroomName ? chatrooms.filter(roomsFilter) : chatrooms}
				/>
			</Sider>
		</React.Fragment>
	);
};

export default LeftSideBar;
