import React from 'react';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { UserObject } from '@/interfaces/model';

interface onlineMemberProps {
	member: UserObject;
}

const OnlineMember = ({ member }: onlineMemberProps) => {
	return (
		<div className="member">
			<Avatar size={32} icon={<UserOutlined />} />
			<p className="member-text">{member.username}</p>
		</div>
	);
};

export default OnlineMember;
