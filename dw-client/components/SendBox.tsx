import React, { useContext, useState } from 'react';
import { Input, message } from 'antd';
import { SendOutlined } from '@ant-design/icons';
import axios from '@/util/apiUtil';
import getLoginHeader from '@/components/utils/getLoginHeader';
import { ChatroomContext, MessageContext } from '@/context';
import { ChatroomContextType, MessageContextType } from '@/context/types';

const sendBoxStyle = {
	display: 'flex',
	flex: '1',
	justifyContent: 'space-evenly',
	alignItems: 'center',
};

const SendBox = () => {
	const { selectedChatroom } = useContext(ChatroomContext) as ChatroomContextType;
	const { setNewMessage } = useContext(MessageContext) as MessageContextType;
	const [input, setInput] = useState('');

	const onClickHandler = async (event: React.MouseEvent<HTMLSpanElement> | React.KeyboardEvent<HTMLInputElement>) => {
		event.preventDefault();

		try {
			if (selectedChatroom) {
				const response = await axios.post(
					`/chatrooms/${selectedChatroom._id}/messages/new`,
					{
						content: input,
					},
					getLoginHeader(),
				);

				setNewMessage(response.data);
				setInput('');
			}
		} catch (err: any) {
			if (err.response) {
				if (err.response.status === 500) {
					message.error(`Server error`, 2);
				}
			} else {
				message.error('Server is unavailable', 2);
			}
		}
	};

	return (
		<div style={{ flex: 1 }}>
			<form style={sendBoxStyle}>
				<Input
					placeholder="Type a message"
					style={{ width: '80%' }}
					value={input}
					onChange={e => setInput(e.target.value)}
					onPressEnter={e => onClickHandler(e)}
				/>
				<SendOutlined style={{ color: '#737373', fontSize: '1rem' }} onClick={e => onClickHandler(e)} />
			</form>
		</div>
	);
};

export default SendBox;
