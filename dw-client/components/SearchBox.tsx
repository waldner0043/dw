import React, { useContext, useState } from 'react';
import { Input } from 'antd';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';
const { Search } = Input;

const searchStyle = {
	display: 'flex',
	justifyContent: 'space-around',
	alignItems: 'center',
	padding: '10px',
	paddingTop: '0px',
};

const SearchBox = () => {
	const { setSearchedChatroomName } = useContext(ChatroomContext) as ChatroomContextType;

	const [chatroomName, setChatroomName] = useState('');

	const onSearchHandler = () => {
		setSearchedChatroomName(chatroomName);
	};

	return (
		<React.Fragment>
			<Search
				style={searchStyle}
				placeholder="Input search text"
				onSearch={onSearchHandler}
				value={chatroomName}
				onChange={e => setChatroomName(e.target.value)}
				enterButton
			/>
		</React.Fragment>
	);
};

export default SearchBox;
