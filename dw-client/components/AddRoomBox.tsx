import React, { CSSProperties, useContext, useState } from 'react';
import { Button, Input, message } from 'antd';
import { UsergroupAddOutlined } from '@ant-design/icons';
import axios from '@/util/apiUtil';
import getLoginHeader from './utils/getLoginHeader';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';

const addRoomBoxStyle: CSSProperties = {
	display: 'flex',
	justifyContent: 'space-evenly',
	alignItems: 'center',
	padding: '10px',
	paddingTop: '0px',
};

const AddRoomBox = () => {
	const [newRoom, setNewRoom] = useState('');
	const { setChatrooms } = useContext(ChatroomContext) as ChatroomContextType;

	const onClickHandler = async (
		event: React.KeyboardEvent<HTMLInputElement> | React.MouseEvent<HTMLSpanElement, MouseEvent>,
	) => {
		event.preventDefault();

		try {
			await axios.post(`chatrooms/new`, { name: newRoom }, getLoginHeader());

			axios
				.get('/chatrooms', getLoginHeader())
				.then(response => {
					setChatrooms(response.data);
				})
				.catch(err => {
					// console.log(err)
					message.error(`Server error`, 2);
				});

			message.success('Create chatroom successfully', 2);
		} catch (err: any) {
			const res = err.response;
			if (res) {
				if (res.status === 400) {
					message.error(`Chatroom's name is duplicated`, 2);
				} else if (res.status === 500) {
					message.error(`Server error`, 2);
				} else if (res.status === 401) {
					message.error(`Unauthorisation`, 2);
				}
			} else {
				message.error('Server is unavailable', 2);
			}
		}

		setNewRoom('');
	};

	return (
		<div style={addRoomBoxStyle}>
			<Input.Group compact>
				<Input
					style={{ width: 'calc(100% - 46px)' }}
					placeholder="Name the Room"
					value={newRoom}
					onChange={e => setNewRoom(e.target.value)}
					onPressEnter={e => onClickHandler(e)}
				/>
				<Button
					type="primary"
					style={{ width: '46px' }}
					icon={<UsergroupAddOutlined onClick={e => onClickHandler(e)} />}
				/>
			</Input.Group>
		</div>
	);
};

export default AddRoomBox;
