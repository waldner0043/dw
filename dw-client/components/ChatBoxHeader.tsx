import React, { CSSProperties, useState, useEffect, useContext } from 'react';
import { MoreOutlined } from '@ant-design/icons';
import { Menu, Dropdown, Layout } from 'antd';
import DeleteRoomModal from './modal/DeleteRoomModal';
import ChangeNameModal from './modal/ChangeNameModal';
import UpdateMemberModal from './modal/UpdateMemberModal';
import LeaveRoomModal from './modal/LeaveRoomModal';
import { ChatroomContext, UserContext } from '@/context';
import { ChatroomContextType, UserContextType } from '@/context/types';
const { Header } = Layout;
// const { Search } = Input;

const headerInfoStyle: CSSProperties = {
	flex: 1,
	margin: '0px',
	letterSpacing: '1px',
};

const iconStyle: CSSProperties = {
	fontSize: '18px',
	color: 'black',
};

interface eventInterface {
	key: string;
}

// type keyType = 'name' | 'leaveRoom' | 'deleteRoom' | 'addMember';

const ChatBoxHeader = () => {
	const { setChatrooms, selectedChatroom, setSelectedChatroom } = useContext(ChatroomContext) as ChatroomContextType;
	const { user } = useContext(UserContext) as UserContextType;

	const [isChangeNameModalVisible, setIsChangeNameModalVisible] = useState(false);
	const [isUpdateMemberModalVisible, setIsUpdateMemberModalVisible] = useState(false);

	const [chatroomTitle, setChatroomTitle] = useState('');
	const [isOwnRoom, setIsOwnRoom] = useState(false);

	useEffect(() => {
		if (selectedChatroom) {
			setChatroomTitle(selectedChatroom.name);
		}

		const userId = user ? user._id : '';
		const ownerId = selectedChatroom ? selectedChatroom.ownerId : '';

		setIsOwnRoom(userId !== '' && userId === ownerId ? true : false);
	}, [user, selectedChatroom]);

	const onClick = async ({ key }: eventInterface) => {
		if (key === 'deleteRoom') {
			DeleteRoomModal(setChatrooms, selectedChatroom, setSelectedChatroom); // function
		} else if (key === 'leaveRoom') {
			LeaveRoomModal(setChatrooms, selectedChatroom, setSelectedChatroom, user); // function
		} else if (key === 'addMember') {
			setIsUpdateMemberModalVisible(true); // set component visible
		} else if (key === 'name') {
			setIsChangeNameModalVisible(true); // set component visible
		}
	};

	const menu = (
		<Menu onClick={onClick}>
			<Menu.Item key="name">Change the room name</Menu.Item>
			<Menu.Item key="addMember">Invite new member</Menu.Item>
			<Menu.Item key={isOwnRoom ? 'deleteRoom' : 'leaveRoom'}>
				{isOwnRoom ? 'Delete the chatroom' : 'Leave the chatroom'}
			</Menu.Item>
		</Menu>
	);

	return (
		<Header className="chatbox-header">
			<h2 style={headerInfoStyle}>{chatroomTitle}</h2>
			<div>
				<Dropdown overlay={menu} trigger={['click']}>
					<MoreOutlined style={iconStyle} onClick={e => e.preventDefault()} />
				</Dropdown>
			</div>
			<ChangeNameModal
				isVisible={isChangeNameModalVisible}
				setIsVisible={setIsChangeNameModalVisible}
				chatroomTitle={chatroomTitle}
				setChatroomTitle={setChatroomTitle}
			/>
			<UpdateMemberModal isVisible={isUpdateMemberModalVisible} setIsVisible={setIsUpdateMemberModalVisible} />
		</Header>
	);
};

export default ChatBoxHeader;
