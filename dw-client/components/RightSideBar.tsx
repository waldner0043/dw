import React, { useContext, useEffect, useState } from 'react';
import OnlineMember from './OnlineMember';
import { Layout } from 'antd';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';
import { UserObject } from '@/interfaces/model';
const { Sider } = Layout;

const RightSideBar = () => {
	const { selectedChatroom, activeMemberIds } = useContext(ChatroomContext) as ChatroomContextType;

	const [isCollapsed, setIsCollapsed] = useState(false);
	const [chatroomMembers, setChatroomMembers] = useState<UserObject[]>([]);
	const [activeMemberList, setActiveMemberList] = useState<UserObject[]>([]);
	const [nonActiveMemberList, setNonActiveMemberList] = useState<UserObject[]>([]);

	useEffect(() => {
		if (selectedChatroom) {
			// prevent duplicated member id
			const filteredMembers = selectedChatroom.members.reduce((accumulated: UserObject[], current) => {
				const isDumplicated = accumulated.find(member => member._id === current._id);
				if (!isDumplicated) {
					return accumulated.concat([current]);
				} else {
					return accumulated;
				}
			}, []);
			setChatroomMembers(filteredMembers);
		}
	}, [selectedChatroom]);

	useEffect(() => {
		const activeMemberIdsSet = new Set(activeMemberIds);
		setActiveMemberList(chatroomMembers.filter(member => activeMemberIdsSet.has(member._id)));
		setNonActiveMemberList(chatroomMembers.filter(member => !activeMemberIdsSet.has(member._id)));
	}, [chatroomMembers, activeMemberIds]);

	const renderedActiveMembers = activeMemberList.map(member => {
		return <OnlineMember member={member} key={member._id} />;
	});
	const renderedNonActiveMembers = nonActiveMemberList.map(member => {
		return <OnlineMember member={member} key={member._id} />;
	});
	const handleOnBreakpoint = (isCollpased: boolean) => {
		setIsCollapsed(isCollpased);
	};

	return (
		<React.Fragment>
			<Sider
				className="right-sider"
				width="20%"
				breakpoint="xl"
				onBreakpoint={handleOnBreakpoint}
				collapsed={isCollapsed}
				collapsedWidth={0}
			>
				<div className="right-sider-content">
					<div className="members-text" style={{ margin: '5px' }}>
						<h1 className="rightSideBar-h1">Online</h1>
					</div>

					{renderedActiveMembers}
					<div className="members-text" style={{ margin: '5px' }}>
						<h1 className="rightSideBar-h1">Offline</h1>
					</div>

					{renderedNonActiveMembers}
				</div>
			</Sider>
		</React.Fragment>
	);
};

export default RightSideBar;
