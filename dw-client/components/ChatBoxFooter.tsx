import React from 'react';
import SendBox from '@/components/SendBox';

const ChatBoxFooter = () => {
	return (
		<footer className="chatbox-footer">
			<SendBox />
		</footer>
	);
};

export default ChatBoxFooter;
