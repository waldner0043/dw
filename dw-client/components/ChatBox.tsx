import React, { useContext } from 'react';
import ChatBoxHeader from './ChatBoxHeader';
import ChatBoxContent from './ChatBoxContent';
import ChatBoxFooter from './ChatBoxFooter';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';

const ChatBox = () => {
	const { selectedChatroom } = useContext(ChatroomContext) as ChatroomContextType;
	return (
		<div>
			{!selectedChatroom ? (
				<div className="welcome-img">
					<img src="image/welcome.png" alt="" />
				</div>
			) : (
				<div>
					<ChatBoxHeader />
					<ChatBoxContent />
					<ChatBoxFooter />
				</div>
			)}
		</div>
	);
};

export default ChatBox;
