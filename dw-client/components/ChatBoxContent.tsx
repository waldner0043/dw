import React, { CSSProperties, useContext } from 'react';
import { format } from 'timeago.js';
import { Layout } from 'antd';
import { MessageContext, UserContext } from '@/context';
import { MessageContextType, UserContextType } from '@/context/types';
const { Content } = Layout;

const chatRecieverStyle: CSSProperties = {
	position: 'relative',
	fontSize: '16px',
	padding: '5px 10px',
	width: 'fit-content',
	borderRadius: '20px',
	backgroundColor: 'white',
	marginBottom: '40px',
};

const chatSenderStyle: CSSProperties = {
	position: 'relative',
	fontSize: '16px',
	padding: '5px 10px',
	width: 'fit-content',
	borderRadius: '20px',
	marginBottom: '40px',
	marginLeft: 'auto',
	backgroundColor: '#dcf8c6',
};

const chatNameStyle: CSSProperties = {
	position: 'absolute',
	top: '-20px',
	left: '1px',
	fontWeight: 700,
	fontSize: 'small',
};

const chatTimestampStyle: CSSProperties = {
	marginLeft: '10px',
	fontSize: 'xx-small',
};

const ChatBoxContent = () => {
	const { messages } = useContext(MessageContext) as MessageContextType;
	const { user } = useContext(UserContext) as UserContextType;

	const renderContents = messages
		.map(message => {
			const userId = user ? user._id : '';
			return (
				<p key={message._id} style={message.senderId._id === userId ? chatSenderStyle : chatRecieverStyle}>
					<span style={chatNameStyle}>{message.senderId.username}</span>
					{message.content}
					<span style={chatTimestampStyle}>{format(message.createdAt)}</span>
				</p>
			);
		})
		.reverse();
	return (
		<div>
			<Content className="chatbox-content">{renderContents}</Content>
		</div>
	);
};

export default ChatBoxContent;
