import React, { useContext } from 'react';
import { Menu } from 'antd';
import { Chatroom } from '@/interfaces/model';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';

const h1Style = {
	margin: '5px',
	paddingLeft: '10px',
	letterSpacing: '1px',
};

const menuStyle = {
	backgroundColor: 'antiquewhite',
};

interface LeftSideBarContentInterface {
	chatroomsMatched: Chatroom[];
}

interface onSelectHandlerInterface {
	key: string;
}

const LeftSideBarContent = ({ chatroomsMatched }: LeftSideBarContentInterface) => {
	const { setSelectedChatroom } = useContext(ChatroomContext) as ChatroomContextType;

	const onSelect = ({ key }: onSelectHandlerInterface) => {
		for (let chatroom of chatroomsMatched) {
			if (chatroom._id === key) {
				setSelectedChatroom(chatroom);
				break;
			}
		}
	};
	const renderedChatRooms = chatroomsMatched.map(chatroom => {
		const { _id, name } = chatroom;
		return (
			<Menu.Item key={_id} style={{ padding: '0px 15px' }}>
				<p className="chatroom-name">{name}</p>
			</Menu.Item>
		);
	});

	return (
		<div className="left-sider-content">
			<h2 className="chatroom-title-name" style={h1Style}>
				Chat Room
			</h2>
			<Menu style={menuStyle} onSelect={onSelect}>
				{renderedChatRooms}
			</Menu>
		</div>
	);
};

export default LeftSideBarContent;
