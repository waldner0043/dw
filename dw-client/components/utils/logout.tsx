import React, { useContext } from 'react';
import { SocketContext, UserContext } from '@/context';
import { SocketContextType, UserContextType } from '@/context/types';
import cookieUtil from '@/util/cookieUtil';
import { ECookieName } from '@/modal/utilModal';
import router from 'next/router';
import { Socket } from 'socket.io-client';
import { ClientToServerEvents, ServerToClientEvents } from '@/interfaces/socket';

const logout = (socket: Socket<ServerToClientEvents, ClientToServerEvents> | null) => {
	if (socket) {
		// socket.emit('leaveRoom', selectedChatroom._id);
		socket.disconnect();
	}

	localStorage.clear();
	cookieUtil.remove(ECookieName.COOKIE_ISLOGIN);
	router.push('/login');
};

export default logout;
