import { Header } from '@/interfaces/axiosClient';

const getLoginHeader = (): Header => {
	const tokenTest = localStorage.getItem('token');
	return {
		headers: {
			'Content-Type': 'application/json',
			Authorization: tokenTest,
		},
	};
};

export default getLoginHeader;
