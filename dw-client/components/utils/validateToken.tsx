const validateToken = () => {
	const expires = localStorage.getItem('expires');
	const now = Date.now();
	console.log('now', now);
	console.log('expires', expires);
	if (expires) {
		console.log(now > parseInt(expires));
	}

	if (expires && now > parseInt(expires)) {
		return false;
	}

	return true;
};

export default validateToken;
