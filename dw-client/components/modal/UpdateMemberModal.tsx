import React, { useContext, useState } from 'react';
import { Modal, Input, message } from 'antd';
import axios from '@/util/apiUtil';
import { Menu } from 'antd';
import getLoginHeader from '../utils/getLoginHeader';
import { Chatroom, UserObject } from '@/interfaces/model';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';

const { Search } = Input;

const menuStyle = {
	backgroundColor: 'white',
};

interface UpdateMemberModalProps {
	isVisible: boolean;
	setIsVisible: (isVisible: boolean) => void;
}

const UpdateMemberModal = ({ isVisible, setIsVisible }: UpdateMemberModalProps) => {
	const { selectedChatroom, setSelectedChatroom, setChatrooms } = useContext(ChatroomContext) as ChatroomContextType;
	const [confirmLoading, setConfirmLoading] = useState(false);
	const [searchName, setSearchName] = useState('');
	const [renderedMembers, setRenderedMembers] = useState([]);
	const [selectedMemberId, setSelectedMemberId] = useState('');
	const [isOkButtonDisable, setIsOkButtonDisable] = useState(true);

	const handleOk = async () => {
		setConfirmLoading(true);

		try {
			if (selectedChatroom) {
				const response = await axios.put(
					`/chatrooms/${selectedChatroom._id}`,
					{
						updateType: 'addMember',
						memberId: selectedMemberId,
					},
					getLoginHeader(),
				);

				axios
					.get('/chatrooms', getLoginHeader())
					.then(res => {
						setChatrooms(res.data);
						const chatroomSelected = res.data.filter(
							(chatroom: Chatroom) => chatroom._id === response.data._id,
						);
						// console.log(chatroomSelected);
						if (chatroomSelected[0]) {
							setSelectedChatroom(chatroomSelected[0]);
						}
					})
					.catch(err => {
						// console.log(err)
						message.error(`Server error`, 2);
					});

				message.success(`${response.data.name} has been deleted successfully!`, 2);
				setSearchName('');
				setRenderedMembers([]);
				setSelectedMemberId('');
			}
		} catch (err: any) {
			if (err.response) {
				if (err.response.status === 500) {
					message.error(`Server error`, 2);
				}
			} else {
				message.error('Server is unavailable', 2);
			}
		}

		setTimeout(() => {
			setIsVisible(false);
			setConfirmLoading(false);
		}, 1000);
	};

	const handleCancel = () => {
		setIsVisible(false);
		setRenderedMembers([]);
		setSelectedMemberId('');
	};

	const handleOnSearch = async () => {
		try {
			const response = await axios.get(`/user/${searchName}`, getLoginHeader());

			if (response.data && response.data.length !== 0) {
				const members = response.data.map((member: UserObject) => {
					const { _id, username } = member;
					return (
						<Menu.Item key={_id}>
							<p>{username}</p>
						</Menu.Item>
					);
				});
				setRenderedMembers(members);
			} else {
				alert('Member is not found');
			}
		} catch (err: any) {
			if (err.response) {
				if (err.response.status === 500) {
					message.error(`Server error`, 2);
				}
			} else {
				message.error('Server is unavailable', 2);
			}
		}
	};

	const handleOnSelect = ({ key }: { key: string }) => {
		setSelectedMemberId(key);
		setIsOkButtonDisable(false);
	};

	return (
		<Modal
			title="Find the member you want to invite and select"
			visible={isVisible}
			onOk={handleOk}
			okButtonProps={{ disabled: isOkButtonDisable }}
			onCancel={handleCancel}
			confirmLoading={confirmLoading}
			okText="Submit"
		>
			<Search
				placeholder="Input the name"
				onSearch={handleOnSearch}
				// style={{ width: 200 }}
				value={searchName}
				onChange={e => setSearchName(e.target.value)}
			/>
			<Menu
				// defaultSelectedKeys={["1"]}
				style={menuStyle}
				onSelect={handleOnSelect}
			>
				{renderedMembers}
			</Menu>
		</Modal>
	);
};

export default UpdateMemberModal;
