import React from 'react';
import { Modal, message } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import axios from '@/util/apiUtil';
import getLoginHeader from '../utils/getLoginHeader';
import { Chatroom, User } from '@/interfaces/model';

const { confirm } = Modal;

const LeaveRoomModal = (
	setChatrooms: (chatrooms: Chatroom[]) => void,
	selectedChatroom: Chatroom | null,
	setSelectedChatroom: (selectedChatroom: Chatroom | null) => void,
	user: User | null,
) => {
	confirm({
		title: 'Do you Want to leave this room?',
		icon: <ExclamationCircleOutlined />,
		content: '',
		async onOk() {
			try {
				if (selectedChatroom) {
					const response = await axios.put(
						`/chatrooms/${selectedChatroom._id}`,
						{
							updateType: 'deleteMember',
							memberId: user ? user._id : '',
						},
						getLoginHeader(),
					);

					axios
						.get('/chatrooms', getLoginHeader())
						.then(response => {
							setChatrooms(response.data);
						})
						.catch(err => {
							if (err.response) {
								if (err.response.status === 500) {
									message.error(`Server error`, 2);
								}
							} else {
								message.error('Server is unavailable', 2);
							}
						});

					setSelectedChatroom(null);
					message.success(
						`${response.data.name} has been removed from ${setSelectedChatroom.name} successfully!`,
						2,
					);
				}
			} catch (err: any) {
				if (err.response) {
					if (err.response.status === 500) {
						message.error(`Server error`, 2);
					}
				} else {
					message.error('Server is unavailable', 2);
				}
			}
		},
		onCancel() {
			console.log(selectedChatroom);
		},
	});
};

export default LeaveRoomModal;
