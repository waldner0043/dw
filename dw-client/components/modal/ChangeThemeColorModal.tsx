import React, { CSSProperties, useState } from 'react';
import { Modal } from 'antd';

const colorChangedStyle: CSSProperties = {
	display: 'flex',
	justifyContent: 'space-between',
	alignItems: 'center',
	margin: '10px',
};

interface ChangeThemeColorModalProps {
	isVisible: boolean;
	setIsVisible: (isVisible: boolean) => void;
}

const ChangeThemeColorModal = ({ isVisible, setIsVisible }: ChangeThemeColorModalProps) => {
	const [leftColor, setLeftColor] = useState();
	const [chatboxColor, setChatboxColor] = useState();
	const [rightColor, setRightColor] = useState();

	const handleOk = async () => {
		setIsVisible(false);
		// ToDo: init theme color
	};

	const handleCancel = () => {
		setIsVisible(false);
	};

	const onLeftColorChanged = e => {
		setLeftColor(e.target.value);
		console.log(e.target.value);
	};
	const onChatboxColorChanged = e => {
		setChatboxColor(e.target.value);
		console.log(e.target.value);
	};
	const onRightColorChanged = e => {
		setRightColor(e.target.value);
		console.log(e.target.value);
	};

	return (
		<Modal
			title="Chat the room name"
			visible={isVisible}
			onOk={handleOk}
			onCancel={handleCancel}
			width={400}
			okText="Submit"
		>
			<div style={colorChangedStyle}>
				<label htmlFor="leftSideBar">LeftSideBar</label>
				<input type="color" name="leftSideBar" value={leftColor} onChange={onLeftColorChanged} />
			</div>

			<div style={colorChangedStyle}>
				<label htmlFor="chatbox">Chatbox</label>
				<input type="color" name="chatbox" value={chatboxColor} onChange={onChatboxColorChanged} />
			</div>

			<div style={colorChangedStyle}>
				<label htmlFor="rightSideBar">RightSideBar</label>
				<input type="color" name="rightSideBar" value={rightColor} onChange={onRightColorChanged} />
			</div>
		</Modal>
	);
};

export default ChangeThemeColorModal;
