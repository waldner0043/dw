import React from 'react';
import { Modal, message } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import axios from '@/util/apiUtil';
import getLoginHeader from '../utils/getLoginHeader';
import { Chatroom } from '@/interfaces/model';

const { confirm } = Modal;

const DeleteRoomModal = (
	setChatrooms: (chatrooms: Chatroom[]) => void,
	selectedChatroom: Chatroom | null,
	setSelectedChatroom: (selectedChatroom: Chatroom | null) => void,
) => {
	confirm({
		title: 'Do you Want to delete this room?',
		icon: <ExclamationCircleOutlined />,
		content: 'All members in the room will be removed and all conversation history will be deleted.',
		async onOk() {
			try {
				if (selectedChatroom) {
					const response = await axios.delete(`/chatrooms/${selectedChatroom._id}`, getLoginHeader());

					axios
						.get('/chatrooms', getLoginHeader())
						.then(response => {
							setChatrooms(response.data);
						})
						.catch(err => {
							// console.log(err)
							message.error(`Server error`, 2);
						});

					setSelectedChatroom(null);
					message.success(`${response.data.name} has been deleted successfully!`, 2);
				}
			} catch (err: any) {
				if (err.response) {
					if (err.response.status === 500) {
						message.error(`Server error`, 2);
					}
				} else {
					message.error('Server is unavailable', 2);
				}
			}
		},
		onCancel() {},
	});
};

export default DeleteRoomModal;
