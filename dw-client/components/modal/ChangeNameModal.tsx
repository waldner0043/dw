import React, { useState, useEffect, useContext } from 'react';
import { Modal, Input, message } from 'antd';
import axios from '@/util/apiUtil';
import getLoginHeader from '../utils/getLoginHeader';
import { ChatroomContext } from '@/context';
import { ChatroomContextType } from '@/context/types';

interface ChangeNameModalProps {
	isVisible: boolean;
	setIsVisible: (isVisible: boolean) => void;
	chatroomTitle: string;
	setChatroomTitle: (title: string) => void;
}

const ChangeNameModal = ({ isVisible, setIsVisible, chatroomTitle, setChatroomTitle }: ChangeNameModalProps) => {
	const { selectedChatroom, setChatrooms } = useContext(ChatroomContext) as ChatroomContextType;
	const [confirmLoading, setConfirmLoading] = useState(false);
	const [newTitle, setNewTitle] = useState(chatroomTitle);

	useEffect(() => {
		setNewTitle(chatroomTitle);
	}, [chatroomTitle]);

	const handleOk = async () => {
		setConfirmLoading(true);

		try {
			if (selectedChatroom) {
				const response = await axios.put(
					`/chatrooms/${selectedChatroom._id}`,
					{
						updateType: 'name',
						name: newTitle,
					},
					getLoginHeader(),
				);

				setChatroomTitle(response.data.name);

				axios
					.get('/chatrooms', getLoginHeader())
					.then(response => {
						setChatrooms(response.data);
					})
					.catch(err => {
						// console.log(err)
						message.error(`Server error`, 2);
					});

				message.success(`${response.data.name} has been changed successfully!`, 2);
			}
		} catch (err: any) {
			const res = err.response;
			if (res) {
				if (res.status === 400) {
					message.error(`Chatroom's name is duplicated`, 2);
				} else if (res.status === 500) {
					message.error(`Server error`, 2);
				}
			} else {
				message.error('Server is unavailable', 2);
			}

			setNewTitle(chatroomTitle);
		}

		setTimeout(() => {
			setIsVisible(false);
			setConfirmLoading(false);
		}, 1000);
	};

	const handleCancel = () => {
		setIsVisible(false);
		setChatroomTitle(chatroomTitle);
	};

	return (
		<Modal
			title="Chat the room name"
			visible={isVisible}
			onOk={handleOk}
			onCancel={handleCancel}
			confirmLoading={confirmLoading}
			okText="Submit"
		>
			<Input value={newTitle} onChange={e => setNewTitle(e.target.value)} />
		</Modal>
	);
};

export default ChangeNameModal;
