import React, { useState, useContext } from 'react';
import {
	UsergroupAddOutlined,
	SearchOutlined,
	UserOutlined,
	LogoutOutlined,
	BgColorsOutlined,
} from '@ant-design/icons';
import SearchBox from '@/components/SearchBox';
import AddRoomBox from '@/components/AddRoomBox';
import { Avatar } from 'antd';
import ChangeThemeColorModel from './modal/ChangeThemeColorModal';
import { SocketContext } from '@/context';
import { SocketContextType } from '@/context/types';
import logout from '@/components/utils/logout';

const headerStyle = {
	display: 'flex',
	justifyContent: 'space-around',
	alignItems: 'center',
	padding: '10px',
};
const headerRightStyle = {
	fontSize: '1.5em',
	display: 'flex',
	justifyContent: 'space-between',
	alignItems: 'center',
	minWidth: '5vw',
	maxWidth: '50%',
	flex: 1,
};

const LeftSideBarHeader = () => {
	const { socket } = useContext(SocketContext) as SocketContextType;
	const [addBtnClicked, setAddBtnClicked] = useState(false);
	const [searchBtnClicked, setSearchBtnClicked] = useState(true);
	const [isChangeThemeModelVisible, setIsChangeThemeModelVisible] = useState(false);

	// const onColorChangedClicked = () => {
	// 	setIsChangeThemeModelVisible(true);
	// };

	const onAddBtnClicked = () => {
		setAddBtnClicked(true);
		setSearchBtnClicked(false);
	};

	const onSearchBtnClicked = () => {
		setSearchBtnClicked(true);
		setAddBtnClicked(false);
	};

	return (
		<div className="left-sider-header" style={{ borderBottom: '1px solid lightgray' }}>
			<div style={headerStyle}>
				<Avatar size={32} icon={<UserOutlined />} />
				<div className="left-sider-header-right" style={headerRightStyle}>
					{/* <BgColorsOutlined onClick={onColorChangedClicked} /> */}
					<SearchOutlined onClick={onSearchBtnClicked} />
					<UsergroupAddOutlined onClick={onAddBtnClicked} />
					<LogoutOutlined onClick={() => logout(socket)} />
				</div>
			</div>
			{searchBtnClicked ? <SearchBox /> : null}
			{addBtnClicked ? <AddRoomBox /> : null}
			<ChangeThemeColorModel isVisible={isChangeThemeModelVisible} setIsVisible={setIsChangeThemeModelVisible} />
		</div>
	);
};

export default LeftSideBarHeader;
