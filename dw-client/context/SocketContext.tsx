import { User } from '@/interfaces/model';
import { ClientToServerEvents, ServerToClientEvents } from '@/interfaces/socket';
import React, { createContext, useState } from 'react';
import { Socket } from 'socket.io-client';

const initialStore: any = {
	socket: null,
};

export const SocketContext = createContext(initialStore);

export const SocketProvider = props => {
	const [socket, setSocket] = useState<Socket<ServerToClientEvents, ClientToServerEvents> | null>(null);
	return <SocketContext.Provider value={{ socket, setSocket }}>{props.children}</SocketContext.Provider>;
};
