import { Chatroom, Message, User } from '@/interfaces/model';
import { ClientToServerEvents, ServerToClientEvents } from '@/interfaces/socket';
import { Socket } from 'socket.io-client';

export type SocketContextType = {
	socket: Socket<ServerToClientEvents, ClientToServerEvents> | null;
	setSocket: (socket: Socket<ServerToClientEvents, ClientToServerEvents> | null) => void;
};

export type ChatroomContextType = {
	selectedChatroom: Chatroom | null;
	setSelectedChatroom: (selectedChatroom: Chatroom | null) => void;
	chatrooms: Chatroom[];
	setChatrooms: (chatrooms: Chatroom[]) => void;
	activeMemberIds: string[];
	setActiveMemberIds: (activeMembers: string[]) => void;
	searchedChatroomName: string;
	setSearchedChatroomName: (searchedChatroomName: string) => void;
};

export type MessageContextType = {
	messages: Message[];
	setMessages: (messages: Message[]) => void;
	newMessage: Message | null;
	setNewMessage: (newMessage: Message | null) => void;
	arrivalMessage: Message | null;
	setArrivalMessage: (arrivalMessage: Message | null) => void;
};

export type UserContextType = {
	isAuth: boolean;
	setIsAuth: (isAuth: boolean) => void;
	user: User | null;
	setUser: (user: User | null) => void;
};
