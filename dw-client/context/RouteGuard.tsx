import { useRouter } from 'next/router';
import React, { useEffect, useLayoutEffect, useState } from 'react';

import cookieUtil from '@/util/cookieUtil';
import eventUtil from '@/util/eventUtil';
import { ECookieName } from '@/modal/utilModal';

const publicPaths = ['/login', '/register'];

const RouteGuard = (props: any) => {
	const { children } = props;
	const router = useRouter();
	const [authorized, setAuthorized] = useState(false);

	const authCheck = (url: string) => {
		// redirect to login page if accessing a private page and not logged ins
		const path = url.split('?')[0];
		if (!cookieUtil.get(ECookieName.COOKIE_ISLOGIN) && !publicPaths.includes(path)) {
			setAuthorized(false);
			router.push({
				pathname: '/login',
				query: { returnUrl: router.asPath },
			});
		} else {
			setAuthorized(true);
		}
	};

	useEffect(() => {
		// on initial load - run auth check
		authCheck(router.asPath);

		// on route change start - hide page content by setting authorized to false
		const hideContent = () => setAuthorized(false);
		router.events.on('routeChangeStart', hideContent);

		// on route change complete - run auth check
		router.events.on('routeChangeComplete', authCheck);

		// unsubscribe from events in useEffect return function
		return () => {
			router.events.off('routeChangeStart', hideContent);
			router.events.off('routeChangeComplete', authCheck);
		};
	}, []);

	return authorized && children;
};

export { RouteGuard };
