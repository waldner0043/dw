import { Chatroom } from '@/interfaces/model';
import React, { createContext, useState } from 'react';

const initialStore: any = {
	selectedChatroom: null,
	chatrooms: [],
	activeMemberIds: [],
	searchedChatroomName: '',
};

export const ChatroomContext = createContext(initialStore);

export const ChatroomProvider = props => {
	const [selectedChatroom, setSelectedChatroom] = useState<Chatroom | null>(null);
	const [chatrooms, setChatrooms] = useState<Chatroom[]>([]);
	const [activeMemberIds, setActiveMemberIds] = useState<string[]>([]);
	const [searchedChatroomName, setSearchedChatroomName] = useState('');

	const valueContext = {
		selectedChatroom,
		setSelectedChatroom,
		chatrooms,
		setChatrooms,
		activeMemberIds,
		setActiveMemberIds,
		searchedChatroomName,
		setSearchedChatroomName,
	};

	return <ChatroomContext.Provider value={valueContext}>{props.children}</ChatroomContext.Provider>;
};
