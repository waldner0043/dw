import { Message } from '@/interfaces/model';
import React, { createContext, useState } from 'react';

export const MessageContext = createContext({});

export const MessageProvider = props => {
	const [messages, setMessages] = useState<Message[]>([]);
	const [newMessage, setNewMessage] = useState<Message | null>(null);
	const [arrivalMessage, setArrivalMessage] = useState<Message | null>(null);
	return (
		<MessageContext.Provider
			value={{
				messages,
				setMessages,
				newMessage,
				setNewMessage,
				arrivalMessage,
				setArrivalMessage,
			}}
		>
			{props.children}
		</MessageContext.Provider>
	);
};
