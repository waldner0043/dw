import { User } from '@/interfaces/model';
import React, { createContext, useState } from 'react';

export const UserContext = createContext({});

export const UserProvider = props => {
	const [user, setUser] = useState<User | null>(null);
	return (
		<UserContext.Provider
			value={{
				user,
				setUser,
			}}
		>
			{props.children}
		</UserContext.Provider>
	);
};
