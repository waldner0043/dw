import '@/styles/css/globals.css';
import '@/styles/css/antd.css';
import '@/styles/css/home.css';
import type { AppProps } from 'next/app';
import React from 'react';
import { RouteGuard } from '@/context/RouteGuard';
import { SocketProvider, UserProvider, ChatroomProvider, MessageProvider } from '@/context';

function MyApp({ Component, pageProps }: AppProps) {
	return (
		<UserProvider>
			<SocketProvider>
				<ChatroomProvider>
					<MessageProvider>
						<RouteGuard>
							<Component {...pageProps} />{' '}
						</RouteGuard>
					</MessageProvider>
				</ChatroomProvider>
			</SocketProvider>
		</UserProvider>
	);
}

export default MyApp;
