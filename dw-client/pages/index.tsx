import React from 'react';
import Home from './home';
import {} from '@/context/UserContext';

const App = () => {
	return (
		<div className="app">
			<Home />
		</div>
	);
};

export default App;
