import React, { useContext } from 'react';
import { message, Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import Link from 'next/link';
import axios from '@/util/apiUtil';
import moment from 'moment';
import { UserContext } from '@/context';
import { UserContextType } from '@/context/types';
import cookieUtil from '@/util/cookieUtil';
import { ECookieName } from '@/modal/utilModal';

const Login = () => {
	const router = useRouter();
	const { setUser } = useContext(UserContext) as UserContextType;

	const onFinish = async ({ username, password, remember }) => {
		// console.log("Received values of form: ", values);

		try {
			const response = await axios.post('/login', {
				username,
				password,
			});

			message.success('Login successfully', 2);

			// const expires = moment().add(response.data.expiresIn);

			localStorage.setItem('token', response.data.token);
			localStorage.setItem('expires', response.data.expiresIn);
			// localStorage.setItem('expires', JSON.stringify(expires.valueOf()));
			localStorage.setItem('user', JSON.stringify({ _id: response.data.user._id }));
			setUser({ _id: response.data.user._id });

			cookieUtil.set(ECookieName.COOKIE_ISLOGIN, 'true');
			router.push('/home');
		} catch (err: any) {
			if (err.response.status === 500) {
				message.error(`Server error`, 2);
			} else {
				message.error(`System error, please restart later`, 2);
			}
		}
	};

	return (
		<div className="login-container">
			<div className="login-subcontainer">
				<div className="login-box">
					<h1 className="login-title">DW Chatroom Login</h1>

					<Form
						name="normal_login"
						className="login-form"
						initialValues={{
							remember: false,
						}}
						onFinish={onFinish}
					>
						<Form.Item
							className="login-form-item"
							name="username"
							rules={[
								{
									required: true,
									message: 'Please input your Username!',
								},
							]}
						>
							<Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
						</Form.Item>
						<Form.Item
							className="login-form-item"
							name="password"
							rules={[
								{
									required: true,
									message: 'Please input your Password!',
								},
							]}
						>
							<Input
								prefix={<LockOutlined className="site-form-item-icon" />}
								type="password"
								placeholder="Password"
							/>
						</Form.Item>
						<Form.Item className="login-form-item login-form-checkbox">
							<Form.Item name="remember" valuePropName="checked" noStyle>
								<Checkbox>Remember me</Checkbox>
							</Form.Item>
						</Form.Item>

						<Form.Item className="login-form-item">
							<Button type="primary" htmlType="submit" className="login-form-button">
								Log in
							</Button>
							<Link href="/register">
								<a className="login-form-register">Register</a>
							</Link>
						</Form.Item>
					</Form>
				</div>
			</div>
		</div>
	);
};

export default Login;
