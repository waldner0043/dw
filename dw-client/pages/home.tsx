import React, { useEffect, useContext } from 'react';
import LeftSideBar from '@/components/LeftSideBar';
import ChatBox from '@/components/ChatBox';
import RightSideBar from '@/components/RightSideBar';
import { Layout } from 'antd';
// import Pusher from "pusher-js";
import axios from '@/util/apiUtil';
import { io } from 'socket.io-client';
import GetLoginHeader from '@/components/utils/getLoginHeader';
import { message } from 'antd';
import { SocketContext, UserContext, ChatroomContext, MessageContext } from '@/context';
import { ChatroomContextType, UserContextType, SocketContextType, MessageContextType } from '@/context/types';
import validateToken from '@/components/utils/validateToken';
import logout from '@/components/utils/logout';

const Home = () => {
	const { user, setUser } = useContext(UserContext) as UserContextType;
	const { selectedChatroom, setChatrooms, activeMemberIds, setActiveMemberIds } = useContext(
		ChatroomContext,
	) as ChatroomContextType;

	// console.log('selectedChatroom', selectedChatroom);

	const { setMessages, newMessage, arrivalMessage, setArrivalMessage } = useContext(
		MessageContext,
	) as MessageContextType;

	const { socket, setSocket } = useContext(SocketContext) as SocketContextType;
	if (!validateToken()) {
		logout(socket);
		message.warning('Session is expired', 2);
	}

	useEffect(() => {
		const user = localStorage.getItem('user');

		if (user) {
			setUser(JSON.parse(user));
		}
	}, []);

	// retrieve all chatrooms by the sepcific user
	useEffect(() => {
		axios
			.get('/chatrooms', GetLoginHeader())
			.then(response => {
				setChatrooms(response.data);
				console.log(response.data);
			})
			.catch(err => {
				if (err.response) {
					if (err.response.status === 500) {
						message.error(`Server error`, 2);
					}
				} else {
					message.error('Server is unavailable', 2);
				}
			});
	}, []);

	// connect to socket.disconnect()
	useEffect(() => {
		const newSocket = io('http://localhost:8900', {
			reconnectionDelayMax: 10000,
			reconnectionAttempts: 3,
		});

		setSocket(newSocket);
	}, []);

	// inform socket connection falied
	// if success, then emit the getMessage event
	useEffect(() => {
		if (socket) {
			socket.io.on('reconnect_attempt', function () {
				console.log('reconnecting');
			});

			socket.io.on('reconnect_failed', function () {
				// console.log('reconnect failed');
				message.error('Socket reconnect failed');
			});
		}

		// get message from chatting room
		if (socket) {
			socket.on('getMessage', data => {
				const message = { ...data };
				console.log('arrivalMsg: ', message);
				setArrivalMessage(message);
			});
		}
		return () => {
			if (socket) {
				// socket.io.removeAllListeners('reconnect_attempt');
				// socket.io.removeAllListeners('reconnect_failed');
				socket.off('getMessage');
			}
		};
	}, [socket]);

	// get all messages within the specific chatroom
	useEffect(() => {
		if (selectedChatroom) {
			axios
				.get(`/chatrooms/${selectedChatroom._id}/messages`, GetLoginHeader())
				.then(response => {
					setMessages(response.data);
				})
				.catch(err => {
					if (err.response) {
						if (err.response.status === 500) {
							message.error(`Server error`, 2);
						}
					} else {
						message.error('Server is unavailable', 2);
					}
				});
		}

		if (selectedChatroom && user) {
			if (socket) {
				const data = {
					chatRoomId: selectedChatroom._id,
					userId: user._id,
				};
				socket.emit('joinRoom', data);

				socket.on('getMembers', users => {
					setActiveMemberIds(users);
					console.log(activeMemberIds);
				});

				return () => {
					socket.emit('leaveRoom', selectedChatroom._id);
					socket.removeAllListeners('getMembers');
				};
			} else {
				message.error('Socket does not exist', 2);
			}
		}
	}, [selectedChatroom]);

	useEffect(() => {
		if (socket) {
			socket.on('removeMember', removedMember => {
				setActiveMemberIds(activeMemberIds.filter(member => member !== removedMember));
			});

			return () => {
				socket.removeAllListeners('removeMember');
			};
		}
	}, [activeMemberIds]);

	// new message for user input
	useEffect(() => {
		if (newMessage) {
			if (socket) {
				setMessages(prev => [...prev, newMessage]);
				socket.emit('sendMessage', newMessage);
			} else {
				message.error('Socket does not exist', 2);
			}
		}
	}, [newMessage]);

	// message from other users
	useEffect(() => {
		arrivalMessage && setMessages(prev => [...prev, arrivalMessage]);
	}, [arrivalMessage, selectedChatroom]);

	return (
		<div className="home">
			<Layout>
				<LeftSideBar />
				<Layout>
					<ChatBox />
				</Layout>
				<RightSideBar />
			</Layout>
		</div>
	);
};

export default Home;

////////////////////////////////////////////////////////////////////////////////////////////////////
// listening chatroomsDB using pusher
// practicing purpose

// useEffect(() => {
//   const pusher = new Pusher("19313fc0aa5b7f33db1c", {
//     cluster: "ap4",
//   });

//   const channel = pusher.subscribe("chatrooms");
//   channel.bind("inserted", (newChatRoom) => {
//     setChatrooms([...chatrooms, newChatRoom]); // keep the existing and add a new one
//   });

//   return () => {
//     channel.unbind_all();
//     channel.unsubscribe();
//   };
// }, [chatrooms]);

///////////////////////////////////////////////////////////////////////////////////////////////////

// const emitDisconnectSocket = (e: any) => {
// 	e.preventDefault();
// 	e.returnValue = '';

// 	console.log(e);

// 	if (socket && selectedChatroom) {
// 		console.log('leaveRoom');
// 		socket.emit('leaveRoom', selectedChatroom._id);
// 	}
// 	if (socket) {
// 		socket.disconnect();
// 	}
// };

// window.addEventListener('beforeunload', emitDisconnectSocket);

// return () => {
// 	window.removeEventListener('beforeunload', emitDisconnectSocket);
// };
