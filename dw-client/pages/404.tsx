const style = { display: 'flex', justifyContent: 'center', padding: '2em' };

export default function Custom404() {
	return (
		<div style={style}>
			<h1 style={{ color: 'red' }}>404 - Page Not Found</h1>
		</div>
	);
}
