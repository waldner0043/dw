import React, { useContext } from 'react';
import { message, Form, Input, Button } from 'antd';
import axios from '@/util/apiUtil';
import moment from 'moment';
import cookieUtil from '@/util/cookieUtil';
import { ECookieName } from '@/modal/utilModal';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { UserContext } from '@/context';
import { UserContextType } from '@/context/types';

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};
const tailFormItemLayout = {
	wrapperCol: {
		xs: {
			span: 24,
			offset: 0,
		},
		sm: {
			span: 16,
			offset: 8,
		},
	},
};

const Register = () => {
	const [form] = Form.useForm();
	const router = useRouter();
	const { setUser } = useContext(UserContext) as UserContextType;

	const onFinish = async ({ email, username, password }) => {
		// console.log("Received values of form: ", values);

		try {
			const response = await axios.post('/register', {
				email,
				username,
				password,
			});
			message.success('Register successfully', 2);
			// const expires = moment().add(response.data.expiresIn);

			localStorage.setItem('token', response.data.token);
			localStorage.setItem('expires', response.data.expiresIn);
			// localStorage.setItem('expires', JSON.stringify(expires.valueOf()));
			localStorage.setItem('user', JSON.stringify({ _id: response.data.user._id }));

			setUser({ _id: response.data.user._id });

			cookieUtil.set(ECookieName.COOKIE_ISLOGIN, 'true');
			router.push('/home');
		} catch (err) {
			if (err.response.status === 500) {
				message.error(`Server error`, 2);
			} else if (err.response.status === 409) {
				message.error(`Username/ Email must be unique`, 2);
			} else {
				message.error(`System error, please restart later`, 2);
			}
		}
	};

	return (
		<div className="register-container">
			<div className="register-subcontainer">
				<h1 className="register-title">DW Chatroom Register</h1>
				<Form
					className="register-form"
					{...formItemLayout}
					form={form}
					name="register"
					onFinish={onFinish}
					scrollToFirstError
				>
					<Form.Item
						className="register-form-item"
						name="username"
						label="Username"
						tooltip="What do you want others to call you?"
						rules={[
							{
								required: true,
								message: 'Please input your nickname!',
								whitespace: true,
							},
						]}
					>
						<Input />
					</Form.Item>

					<Form.Item
						className="register-form-item"
						name="password"
						label="Password"
						rules={[
							{
								required: true,
								message: 'Please input your password!',
							},
						]}
						hasFeedback
					>
						<Input.Password />
					</Form.Item>

					<Form.Item
						className="register-form-item"
						name="confirm"
						label="Confirm"
						dependencies={['password']}
						hasFeedback
						rules={[
							{
								required: true,
								message: 'Please confirm your password!',
							},
							({ getFieldValue }) => ({
								validator(_, value) {
									if (!value || getFieldValue('password') === value) {
										return Promise.resolve();
									}

									return Promise.reject(
										new Error('The two passwords that you entered do not match!'),
									);
								},
							}),
						]}
					>
						<Input.Password />
					</Form.Item>

					<Form.Item
						className="register-form-item"
						name="email"
						label="E-mail"
						rules={[
							{
								type: 'email',
								message: 'The input is not valid E-mail!',
							},
							{
								required: true,
								message: 'Please input your E-mail!',
							},
						]}
					>
						<Input />
					</Form.Item>

					<Form.Item {...tailFormItemLayout} className="register-form-item">
						<div className="register-form-button-container">
							<Button type="primary" htmlType="submit" className="register-form-button">
								Register
							</Button>
							<Link href="/login">
								<a className="login-form-register">Back</a>
							</Link>
						</div>
					</Form.Item>
				</Form>
			</div>
		</div>
	);
};

export default Register;
