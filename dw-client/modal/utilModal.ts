export enum ECookieName {
	COOKIE_ISLOGIN = 'isLogin',
}

export enum EHttpMethod {
	GET = 'GET',
	POST = 'POST',
}
