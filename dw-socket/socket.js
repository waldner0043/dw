const io = require('socket.io')(8900, {
	cors: {
		origin: 'http://localhost:3000', // the port of request from client server
	},
});

// user[socketId] = {chatRoomId: id, userId: id}
const user = {};

io.on('connection', socket => {
	// connect init
	console.log('A user connected.');
	user[socket.id] = {};

	// send and get message
	socket.on('sendMessage', data => {
		console.log(data.chatRoomId, data.content);
		socket.to(data.chatRoomId).emit('getMessage', data);
	});

	// join the room
	socket.on('joinRoom', async ({ userId, chatRoomId }) => {
		// console.log(`${socket.id} join ${chatRoomId}`);

		try {
			addUser(socket.id, chatRoomId, userId);

			socket.userId = userId;
			await socket.join(chatRoomId);

			const rooms = io.of('/').adapter.rooms;
			const clients = rooms.get(chatRoomId);

			const members = [];

			for (const clientId of clients) {
				const clientSocket = io.sockets.sockets.get(clientId);
				members.push(clientSocket.userId);
			}
			io.to(chatRoomId).emit('getMembers', members);
		} catch (err) {
			console.log('error', error.message);
			io.to(socket.id).emit('error', { code: 500, message: error.message });
		}
	});

	// leave the room
	socket.on('leaveRoom', async chatRoomId => {
		console.log(`${socket.id} leave ${chatRoomId}`);

		try {
			updateUser(socket.id, '');

			await socket.leave(chatRoomId);
			const member = io.sockets.sockets.get(socket.id).userId;

			io.to(chatRoomId).emit('removeMember', member);
		} catch (err) {
			console.log('error', error.message);
			io.to(socket.id).emit('error', { code: 500, message: error.message });
		}
	});

	// disconnect when the browser reload or shut down
	socket.on('disconnect', () => {
		console.log('A user disconnected');
		removerUser(socket.id);
	});

	const addUser = (socketId, chatRoomId, userId) => {
		if (user[socketId]) {
			user[socketId].chatRoomId = chatRoomId;
			user[socketId].userId = userId;
		} else {
			user[socketId] = { chatRoomId: chatRoomId, userId: userId };
		}
	};

	const updateUser = (socketId, chatRoomId) => {
		if (user[socketId]) {
			user[socketId].chatRoomId = chatRoomId;
		} else {
			user[socketId] = { chatRoomId: chatRoomId };
		}
	};

	const removerUser = socketId => {
		const chatRoomId = user[socketId].chatRoomId;
		const userId = user[socketId].userId;

		if (chatRoomId !== '') {
			io.to(chatRoomId).emit('removeMember', userId);
		}

		delete user[socketId];
	};
});
