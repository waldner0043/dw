import passport from 'passport';

const validateToken = passport.authenticate('jwt', { session: false });

export default validateToken;
