// importing
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import passport from 'passport';
import passportConfigJWT from './config/passport.js';
import LocalStrategy from 'passport-local';
import User from './models/users.js';
import messageRouter from './route/message.js';
import userRouter from './route/user.js';
import chatRoomRouter from './route/chatRoom.js';
// import Pusher from "pusher";

// app config
const app = express();
const PORT = process.env.PORT || 3080;
const connectionUrl =
	'mongodb+srv://admin:mRs9TOwKJh96YNOX@cluster0.gmrz3.mongodb.net/dwchatappdb?retryWrites=true&w=majority';
// const connectionUrl = "mongodb://localhost:27017/chatApp";

// middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(passport.initialize());

passport.use(new LocalStrategy(User.authenticate()));
passportConfigJWT(passport);

passport.serializeUser(User.serializeUser()); // how to we store user in a session
passport.deserializeUser(User.deserializeUser());

// app.use("*", authUser); // hardcode a user

// db config

mongoose.connect(connectionUrl);

const db = mongoose.connection;

db.once('open', () => {
	console.log('DB connected');
});

// api routes
app.get('/', (req, res) => {
	res.status(200).send('<h1>Hello world</h1>');
});

app.use('/api', userRouter);
app.use('/api/chatrooms', chatRoomRouter);
app.use('/api/chatrooms/:id/messages', messageRouter);

app.all('*', (req, res, next) => {
	res.status(404).send('Page Not Found');
});

// listener
app.listen(PORT, () => {
	console.log(`listening on port: ${PORT}`);
});

////////////////////////////////////////////////////////////////////
/**
 * create pusher listener (for listening the mongodb data changed)
 */
//  const pusher = new Pusher({
//   appId: "1319721",
//   key: "19313fc0aa5b7f33db1c",
//   secret: "f817551edaa4321c2c26",
//   cluster: "ap4",
//   useTLS: true,
// });

// listening the mongodb chatroom data changed while it is opening
// practicing purpose

// db.once("open", () => {
//   console.log("DB connected");

//   const chatroomCollection = db.collection("chatrooms");

//   const pipeline = [
//     {
//       $match: {
//         operationType: "insert",
//       },
//     },
//   ];

//   const changeStream = chatroomCollection.watch(pipeline);

//   changeStream.on("change", (change) => {
//     console.log(change);
//     const chatroomDetails = change.fullDocument;
//     pusher.trigger("chatrooms", "inserted", chatroomDetails);

//     if (change.operationType === "insert") {
//       const chatroomDetails = change.fullDocument;
//       pusher.trigger("chatrooms", "inserted", chatroomDetails);
//     } else {
//       console.log("Error triggering Pusher");
//     }
//   });
// });
