import mongoose from 'mongoose';
import messages from './messages.js';
const Schema = mongoose.Schema;

const ChatRoomSchema = new Schema(
	{
		name: {
			type: String,
			required: true,
			unique: true,
		},
		members: [
			{
				type: Schema.Types.ObjectId,
				ref: 'users',
			},
		],
		ownerId: {
			type: Schema.Types.ObjectId,
			ref: 'users',
		},
	},
	{ timestamps: true },
);

ChatRoomSchema.post('findOneAndDelete', async doc => {
	if (doc) {
		try {
			await messages.deleteMany({
				chatRoomId: doc._id,
			});
		} catch (err) {
			console.log(err);
		}
	}
});

export default mongoose.model('chatRooms', ChatRoomSchema);
