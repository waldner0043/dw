import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const MessageSchema = new Schema(
	{
		chatRoomId: {
			type: Schema.Types.ObjectId,
			ref: 'chatRooms',
			required: true,
		},
		senderId: {
			type: Schema.Types.ObjectId,
			ref: 'users',
			required: true,
		},
		content: {
			type: String,
			required: true,
		},
	},
	{ timestamps: true },
);

// collection
export default mongoose.model('messages', MessageSchema);
