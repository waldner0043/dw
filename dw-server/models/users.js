import mongoose from 'mongoose';
import chatRooms from './chatRooms.js';
import passportLocalMongoose from 'passport-local-mongoose';
import messages from './messages.js';

const Schema = mongoose.Schema;

const UserSchema = new Schema({
	email: {
		type: String,
		required: true,
		unique: true,
	},
});

UserSchema.post('findOneAndDelete', async doc => {
	if (doc) {
		// remove messages if the chatroom owner is the removed user
		removeMessagesInRemovedChatroom(doc._id);

		try {
			// remove user owns chatroom
			await chatRooms.deleteMany({
				ownerId: doc._id,
			});

			// remove user is the members in the chatroom
			await chatRooms.updateMany({}, { $pull: { members: doc._id } });
		} catch (err) {
			console.log(err);
		}
	}
});

UserSchema.plugin(passportLocalMongoose);

export default mongoose.model('users', UserSchema);

const removeMessagesInRemovedChatroom = async id => {
	try {
		const chatRoomArray = await chatRooms.find({ ownerId: id });
		console.log(chatRoomArray);
		chatRoomArray.map(async chatRoom => {
			await messages.deleteMany({
				chatRoomId: chatRoom._id,
			});
		});
	} catch (err) {
		console.log(err);
	}
};
