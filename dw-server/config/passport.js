import fs from 'fs';
import path from 'path';
import User from '../models/users.js';
import { Strategy as JwtStrtegy, ExtractJwt } from 'passport-jwt';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const pathToKey = path.join(__dirname, '..', 'id_rsa_pub.pem');
const PUB_KEY = fs.readFileSync(pathToKey, 'utf8');

// verify piece of JWT
const options = {};
options.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
options.secretOrKey = PUB_KEY;
options.algorithms = ['RS256'];

// already know the jwt is valid
const strategy = new JwtStrtegy(options, (payload, done) => {
	User.findOne({ _id: payload.sub })
		.then(user => {
			if (user) {
				return done(null, user);
			} else {
				return done(null, false);
			}
		})
		.catch(err => {
			done(err, null);
		});
});

export default passport => {
	passport.use(strategy);
};
