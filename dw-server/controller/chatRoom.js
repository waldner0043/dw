import e from 'express';
import ChatRoom from '../models/chatRooms.js';
import User from '../models/users.js';

/**
 * find all chatrooms by userId
 *
 * @param {user:{_id}} req
 * @param {ChatRoom} res
 */
const findAllChatRoomsByUser = (req, res, next) => {
	const userId = req.user._id;
	ChatRoom.find({ $or: [{ ownerId: userId }, { members: userId }] })
		.populate('members')
		.exec((err, data) => {
			if (err) {
				res.status(500).send(err);
			} else {
				res.status(200).send(data);
			}
		});
};

/**
 * create a chatroom by userId
 *
 * @param {user:{_id}, body:{name}} req
 * @param {ChatRoom} res
 */
const createChatRoom = async (req, res, next) => {
	const newChatRoom = new ChatRoom(req.body);
	newChatRoom.ownerId = req.user._id;
	newChatRoom.members = [req.user._id];

	try {
		const savedChatRoom = await newChatRoom.save();

		return res.status(201).send(savedChatRoom);
	} catch (err) {
		if (err.code === 11000) {
			return res.status(400).send(err);
		}

		res.status(500).send(err);
	}
};

/**
 * create a chatroom by userId
 *
 * @param {user:{_id}, params:{id}, body:{updateType, name||memberId}} req
 * @param {ChatRoom} res
 */
const updateChatRoom = (req, res, next) => {
	const { updateType } = req.body;
	if (updateType === 'name') {
		editName(req, res, next);
	} else if (updateType === 'addMember') {
		addMemberToChatRoom(req, res, next);
	} else if (updateType === 'deleteMember') {
		deleteMemberFromChatRoom(req, res, next);
	} else {
		res.status(500).send('Update failure: Request data is incomplete');
	}
};

/**
 * create a chatroom by userId
 *
 * @param {params:{id}} req
 * @param {ChatRoom} res
 */
const deleteChatRoom = async (req, res, next) => {
	const { id } = req.params;
	try {
		const deletedChatRoom = await ChatRoom.findByIdAndDelete(id);
		res.status(201).send(deletedChatRoom);
	} catch (err) {
		res.status(500).send(err);
	}
};

// function for updateChatRoom
const editName = async (req, res, next) => {
	const { id } = req.params;

	try {
		const updatedChatRoom = await ChatRoom.findByIdAndUpdate(id, req.body, {
			new: true,
		});

		res.status(201).send(updatedChatRoom);
	} catch (err) {
		console.log(err);
		if (err.code === 11000) {
			return res.status(400).send(err);
		}

		res.status(500).send(err);
	}
};

// function for updateChatRoom
const addMemberToChatRoom = async (req, res, next) => {
	try {
		const chatRoom = await ChatRoom.findById(req.params.id);
		const user = await User.findById(req.body.memberId); // newMemberId
		chatRoom.members.push(user);

		const savedChatRoom = await chatRoom.save();
		res.status(201).send(savedChatRoom);
	} catch (err) {
		res.status(500).send(err);
	}
};

// function for updateChatRoom
const deleteMemberFromChatRoom = async (req, res, next) => {
	const chatRoomId = req.params.id;
	const userId = req.body.memberId;
	try {
		const savedChatRoom = await ChatRoom.findByIdAndUpdate(
			chatRoomId,
			{
				$pull: { members: userId },
			},
			{
				new: true,
			},
		);
		res.status(201).send(savedChatRoom);
	} catch (err) {
		res.status(500).send(err);
	}
};

export default {
	findAllChatRoomsByUser,
	createChatRoom,
	updateChatRoom,
	deleteChatRoom,
};
