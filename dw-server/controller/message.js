import Message from '../models/messages.js';
import ChatRoom from '../models/chatRooms.js';
import User from '../models/users.js';

/**
 * find all msgs inside the specific chatroom by chatroomId
 *
 * @param {params:{id}} req
 * @param {[Message]} res
 */
const findAllMessagesByChatRoom = (req, res, next) => {
	Message.find({ chatRoomId: req.params.id })
		.populate('senderId') // populate the ref doc
		.exec((err, data) => {
			if (err) {
				res.status(500).send(err);
			} else {
				res.status(200).json(data);
			}
		});
};

/**
 * create the message in given chatroom
 *
 * @param {user:{_id}, params:{id}, body:{content}} req
 * @param {Message} res
 */
const createMessage = async (req, res, next) => {
	try {
		const chatRoom = await ChatRoom.findById(req.params.id);
		const user = await User.findById(req.user._id);

		const newMessage = new Message({
			chatRoomId: chatRoom,
			senderId: user,
			content: req.body.content,
		});

		const savedMessage = await newMessage.save();
		savedMessage.chatRoomId = savedMessage.chatRoomId._id;
		console.log(savedMessage);
		res.status(201).send(savedMessage);
	} catch (err) {
		res.status(500).send(err);
	}
};

/**
 * delete msgs by messageId
 *
 * @param {params:{messageId}} req
 * @param {Message} res
 */
const deleteMessage = async (req, res, next) => {
	const { messageId } = req.params;
	try {
		const deletedMessage = await Message.findByIdAndDelete(messageId);
		res.status(201).send(deletedMessage);
	} catch (err) {
		res.status(500).send(err);
	}
};

export default {
	findAllMessagesByChatRoom,
	createMessage,
	deleteMessage,
};
