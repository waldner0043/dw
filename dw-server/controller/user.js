import passport from 'passport';
import User from '../models/users.js';
import issueJWT from '../utils/issueJWT.js';

/**
 * @param {body:{usename, email, password}} req
 * @param {User} res
 */
const createUser = async (req, res, next) => {
	try {
		const { email, username, password } = req.body;
		const user = new User({ email, username });
		const registeredUser = await User.register(user, password);

		const jwt = issueJWT(registeredUser);
		res.status(201).send({
			user: registeredUser,
			token: jwt.token,
			expiresIn: jwt.expires,
		});
	} catch (err) {
		if (err.code === 11000) {
			return res.status(400).send(err);
		} else if (err.message === 'A user with the given username is already registered') {
			return res.status(409).send(err);
		}

		res.status(500).send(err);
	}
};

/**
 * @param {user:{_id}} req
 * @param {User} res
 */
const deleteUser = async (req, res, next) => {
	const userId = req.user._id;

	try {
		const deletedUser = await User.findByIdAndDelete(userId);
		res.status(201).send(deletedUser);
	} catch (err) {
		res.status(500).send(err);
	}
};

/**
 * @param {params:{name}} req
 * @param {User} res
 */
const findUserByName = async (req, res, next) => {
	const { name } = req.params;

	try {
		const user = await User.find({ username: name });
		res.status(200).send(user);
	} catch (err) {
		res.status(500).send(err);
	}
};

/**
 * @param {body:{usename, password}} req
 * @param {User} res
 */
const loginUser = (req, res, next) => {
	passport.authenticate('local', { session: false }, (err, user, info) => {
		if (err) {
			return next(err);
		}

		if (!user) {
			return res.status(401).send({ message: `User Not Found, password or username is incorrect` });
		}

		const jwt = issueJWT(user);
		return res.status(200).send({
			user: user,
			token: jwt.token,
			expiresIn: jwt.expires,
		});

		// // bad request
		// return res.status(400).send(info);
	})(req, res, next);
};

export default { createUser, deleteUser, findUserByName, loginUser };
