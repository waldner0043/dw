import express from 'express';
import verifyJWT from '../utils/verifyJWT.js';
import msgController from '../controller/message.js';

const router = express.Router({ mergeParams: true });

// success
router.get('/', verifyJWT, msgController.findAllMessagesByChatRoom);

// success
router.post('/new', verifyJWT, msgController.createMessage);

//success
router.delete('/:messageId', verifyJWT, msgController.deleteMessage);

export default router;
