import express from 'express';
import verifyJWT from '../utils/verifyJWT.js';
import chatRoomController from '../controller/chatRoom.js';

const router = express.Router();

// Success
router.route('/').get(verifyJWT, chatRoomController.findAllChatRoomsByUser);

// Success
router.post('/new', verifyJWT, chatRoomController.createChatRoom);

// Success
router
	.route('/:id')
	.put(verifyJWT, chatRoomController.updateChatRoom)
	.delete(verifyJWT, chatRoomController.deleteChatRoom);

export default router;
