import express from 'express';
import verifyJWT from '../utils/verifyJWT.js';
import userController from '../controller/user.js';

const router = express.Router();

// success
router.get('/user/:name', verifyJWT, userController.findUserByName);

// success
router.post('/register', userController.createUser);

// success
router.delete('/unregister', userController.deleteUser);

// success
router.post('/login', userController.loginUser);

// success testing jwtToken purpose
router.get('/protected', verifyJWT, (req, res, nest) => {
	res.status(200).send({ id: req.user._id, message: 'you are authorised' });
});

export default router;
